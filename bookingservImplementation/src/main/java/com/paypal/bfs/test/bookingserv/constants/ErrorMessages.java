package com.paypal.bfs.test.bookingserv.constants;

public class ErrorMessages {
	public static final String DEPOSIT_NEGATIVE_MESSAGE = "deposit can't have negative value.";
	public static final String TOTAL_PRICE_NEGATIVE_MESSAGE = "totalPrice can't have negative value.";
}
