package com.paypal.bfs.test.bookingserv.service.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.paypal.bfs.test.bookingserv.api.model.Address;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.model.AddressEntity;
import com.paypal.bfs.test.bookingserv.model.BookingEntity;
import com.paypal.bfs.test.bookingserv.repository.BookingRepository;
import com.paypal.bfs.test.bookingserv.service.BookingService;

@SpringBootTest
public class BookingServiceTest {
	@Mock
	private BookingRepository bookingRepository;
	
	private BookingService bookingService;
	
	@BeforeEach
	void initUseCase() {
		bookingService = new BookingService(bookingRepository);
	}
	
	@Test
	public void getAllBookingsTest() {
		List<Booking> bookings = bookingService.getAllBookings();
		assertEquals(0, bookings.size());
		
		Mockito.when(bookingRepository.findAll()).thenReturn(getValidBookingList());
		bookings = bookingService.getAllBookings();
		
		assertEquals(1, bookings.size());
	}
	
	@Test
	public void addBookingTest() {
		Booking booking = getValidBooking();
		Mockito.when(bookingRepository.save(Mockito.any(BookingEntity.class))).thenReturn(getValidBookingEntity());
		Optional<BookingEntity> bookingEntityOp = bookingService.addBooking(booking);
		assertTrue(bookingEntityOp.isPresent());
	}
	
	private List<BookingEntity> getValidBookingList() {
		List<BookingEntity> bookings = new ArrayList<>();
		bookings.add(getValidBookingEntity());
		return bookings;
	}

	private BookingEntity getValidBookingEntity() {
		// Creating the booking
		BookingEntity bookingEntity = new BookingEntity();
		bookingEntity.setCheckin(false);
		bookingEntity.setCheckout(false);
		bookingEntity.setDateOfBirth(new Date(System.currentTimeMillis()));
		bookingEntity.setDeposit(1000.0);
		bookingEntity.setFirstName("Ankit");
		bookingEntity.setId(1);
		bookingEntity.setLastName("Sood");
		bookingEntity.setTotalprice(2500.0);

		// Creating the address
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setId(1);
		addressEntity.setCity("Bangalore");
		addressEntity.setCountry("India");
		addressEntity.setLine1("Savior City");
		addressEntity.setState("Karnataka");
		addressEntity.setZipCode("560103");
		bookingEntity.setAddress(addressEntity);

		return bookingEntity;
	}
	
	private Booking getValidBooking() {
		// Creating the booking
		Booking booking = new Booking();
		booking.setCheckin(false);
		booking.setCheckout(false);
		booking.setDateOfBirth(new Date(System.currentTimeMillis()));
		booking.setDeposit(1000.0);
		booking.setFirstName("Ankit");
		booking.setId(1);
		booking.setLastName("Sood");
		booking.setTotalprice(2500.0);

		// Creating the address
		Address address = new Address();
		address.setId(1);
		address.setCity("Bangalore");
		address.setCountry("India");
		address.setLine1("Savior City");
		address.setState("Karnataka");
		address.setZipCode("560103");
		booking.setAddress(address);

		return booking;
	}
}
