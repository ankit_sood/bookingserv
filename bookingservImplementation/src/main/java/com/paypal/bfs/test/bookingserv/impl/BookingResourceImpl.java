package com.paypal.bfs.test.bookingserv.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.constants.ErrorMessages;
import com.paypal.bfs.test.bookingserv.model.BookingEntity;
import com.paypal.bfs.test.bookingserv.service.BookingService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BookingResourceImpl implements BookingResource {
	private final BookingService bookingService;

	@Override
    public ResponseEntity<?> create(Booking booking) {
    	log.debug("Request Received to create the booking. Name: {}",booking.getFirstName()+booking.getLastName());
    	List<String> errorMessages = validateBooking(booking);
    	if(!CollectionUtils.isEmpty(errorMessages)) {
    		log.debug("Issues found while validating. Rejecting the payload.");
    		return ResponseEntity.badRequest().body(errorMessages);
    	}
    	log.debug("No Issues found while validating. Creating a Booking.");
    	Optional<BookingEntity> bookingEntity = this.bookingService.addBooking(booking);
    	if(bookingEntity.isPresent()) {
    		log.debug("Booking created Successfully with id: {}"+bookingEntity.get().getId());
    		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
					  .path("/{id}")
					  .buildAndExpand(bookingEntity.get().getId()).toUri();
    		return ResponseEntity.created(location).build();
    	} else {
    		return ResponseEntity.noContent().build();
    	}
    }
    
	@Override
	public ResponseEntity<List<Booking>> get() {
		log.debug("Request Received to fetch all the bookings.");
		return ResponseEntity.ok(bookingService.getAllBookings());
	}
	
	private List<String> validateBooking(Booking booking) {
		List<String> errorMessages = new ArrayList<>();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Booking>> violations = validator.validate(booking);
		for (ConstraintViolation<Booking> violation : violations) {
			errorMessages.add(violation.getPropertyPath()+" "+violation.getMessage());
		}
		if(booking.getDeposit()<0.0) {
			errorMessages.add(ErrorMessages.DEPOSIT_NEGATIVE_MESSAGE);
		}
		if(booking.getTotalprice()<0.0) {
			errorMessages.add(ErrorMessages.TOTAL_PRICE_NEGATIVE_MESSAGE);
		}
		return errorMessages;
	}
}
