package com.paypal.bfs.test.bookingserv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BookingControllerAdvice {
	
	@ExceptionHandler(RuntimeException.class)
	ResponseEntity<ErrorMessage> handleAllExceptions(RuntimeException ex) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getErrorMessage(ex));
	}
	
	private ErrorMessage getErrorMessage(Exception ex) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage("Unexpected Error Occurred");
		errorMessage.setSuccess(false);
		errorMessage.setCause(ex.getMessage());
		return errorMessage;
	}
}
