package com.paypal.bfs.test.bookingserv.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paypal.bfs.test.bookingserv.model.BookingEntity;

public interface BookingRepository extends JpaRepository<BookingEntity, Integer>{
	
	//List<BookingEntity> findAll()
}
