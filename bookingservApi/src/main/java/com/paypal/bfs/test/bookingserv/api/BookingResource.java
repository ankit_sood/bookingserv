package com.paypal.bfs.test.bookingserv.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.paypal.bfs.test.bookingserv.api.model.Booking;

public interface BookingResource {
    /**
     * Create {@link Booking} resource
     *
     * @param booking the booking object
     * @return the created booking
     */
    @PostMapping("/v1/bfs/booking")
    ResponseEntity<?> create(@RequestBody Booking booking) throws Exception;

    
    /**
     * Get resource
     *
     * @return List of all the bookings
     */
    @GetMapping("/v1/bfs/booking")
    ResponseEntity<List<Booking>> get();
}
