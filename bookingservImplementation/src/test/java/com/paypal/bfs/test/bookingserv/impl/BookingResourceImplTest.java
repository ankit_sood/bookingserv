package com.paypal.bfs.test.bookingserv.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.api.model.Address;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.constants.ErrorMessages;
import com.paypal.bfs.test.bookingserv.model.BookingEntity;
import com.paypal.bfs.test.bookingserv.service.BookingService;

@SpringBootTest
public class BookingResourceImplTest {
	@Mock
	private BookingService bookingService;

	private BookingResource bookingResource;

	@BeforeEach
	void initUseCase() {
		bookingResource = new BookingResourceImpl(bookingService);
	}

	@Test
	void getAllBookingsTest() {
		List<Booking> bookings = getValidBookingList();
		Mockito.when(bookingService.getAllBookings()).thenReturn(bookings);
		ResponseEntity<List<Booking>> response = bookingResource.get();
		// Check if response is null
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		// Check if response has some booking
		List<Booking> responseBookings = response.getBody();
		assertEquals(1, responseBookings.size());

		// Check to match the values
		Booking actualBooking = responseBookings.get(0);
		assertEquals(bookings.get(0).toString(), actualBooking.toString());
	}

	@Test
	void createBookingTest() throws Exception {
		Booking booking = getValidBooking();
		// Prepare booking entity
		BookingEntity bookingEntity = new BookingEntity();
		bookingEntity.setId(1);
		Optional<BookingEntity> bookingEntityOp = Optional.of(bookingEntity);

		Mockito.when(bookingService.addBooking(booking)).thenReturn(bookingEntityOp);

		ResponseEntity<?> response = bookingResource.create(booking);
		// Check if response is null
		assertNotNull(response);

		// Check if response has some booking
		assertEquals(HttpStatus.CREATED, response.getStatusCode());

	}

	@SuppressWarnings("unchecked")
	@Test
	void createBookingValidationFailures() throws Exception {
		Booking booking = getBookingWithoutName();
		ResponseEntity<?> response = bookingResource.create(booking);

		// Check if response is null
		assertNotNull(response);
		
		// Check if response has some booking
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
		List<String> errors = (List<String>) response.getBody();
		assertEquals(4, errors.size());
		
		assertTrue(getErrorMesages().containsAll(errors));
		//errors.containsAll(errors);
	}

	private List<Booking> getValidBookingList() {
		List<Booking> bookings = new ArrayList<>();
		bookings.add(getValidBooking());
		return bookings;
	}

	private Booking getValidBooking() {
		// Creating the booking
		Booking booking = new Booking();
		booking.setCheckin(false);
		booking.setCheckout(false);
		booking.setDateOfBirth(new Date(System.currentTimeMillis()));
		booking.setDeposit(1000.0);
		booking.setFirstName("Ankit");
		booking.setId(1);
		booking.setLastName("Sood");
		booking.setTotalprice(2500.0);

		// Creating the address
		Address address = new Address();
		address.setId(1);
		address.setCity("Bangalore");
		address.setCountry("India");
		address.setLine1("Savior City");
		address.setState("Karnataka");
		address.setZipCode("560103");
		booking.setAddress(address);

		return booking;
	}

	private Booking getBookingWithoutName() {
		// Creating the booking
		Booking booking = new Booking();
		booking.setCheckin(false);
		booking.setCheckout(false);
		booking.setLastName("");
		booking.setDateOfBirth(new Date(System.currentTimeMillis()));
		booking.setDeposit(-1.0);
		booking.setId(1);
		booking.setTotalprice(-2500.0);

		// Creating the address
		Address address = new Address();
		address.setId(1);
		address.setCity("Bangalore");
		address.setCountry("India");
		address.setLine1("Savior City");
		address.setState("Karnataka");
		address.setZipCode("560103");
		booking.setAddress(address);

		return booking;
	}
	
	private List<String> getErrorMesages(){
		List<String> errors = new ArrayList<>();
		errors.add("firstName must not be null");
		errors.add("lastName size must be between 1 and 255");
		errors.add(ErrorMessages.DEPOSIT_NEGATIVE_MESSAGE);
		errors.add(ErrorMessages.TOTAL_PRICE_NEGATIVE_MESSAGE);
		return errors;
	}
}
