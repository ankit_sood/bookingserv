package com.paypal.bfs.test.bookingserv.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.paypal.bfs.test.bookingserv.api.model.Address;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.model.AddressEntity;
import com.paypal.bfs.test.bookingserv.model.BookingEntity;
import com.paypal.bfs.test.bookingserv.repository.BookingRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BookingService {
	private final BookingRepository bookingRepository;
	
	@Transactional
	public List<Booking> getAllBookings(){
		List<Booking> bookingsList = new ArrayList<>();
		
		List<BookingEntity> bookings = bookingRepository.findAll();
		if(!CollectionUtils.isEmpty(bookings)) {
			for(BookingEntity bookingEntity: bookings) {
				bookingsList.add(convert(bookingEntity));
			}
		}
		return bookingsList;
	}
	
	public Optional<BookingEntity> addBooking(Booking booking) {
		return Optional.ofNullable(bookingRepository.save(convert(booking)));
	}
	
	private Booking convert(BookingEntity bookingEntity) {
		Booking booking = new Booking();
		booking.setCheckin(bookingEntity.getCheckin());
		booking.setCheckout(bookingEntity.getCheckout());
		booking.setDateOfBirth(bookingEntity.getDateOfBirth());
		booking.setDeposit(bookingEntity.getDeposit());
		booking.setFirstName(bookingEntity.getFirstName());
		booking.setId(bookingEntity.getId());
		booking.setLastName(bookingEntity.getLastName());
		booking.setTotalprice(bookingEntity.getTotalprice());
		if(bookingEntity.getAddress()!=null) {
			booking.setAddress(convert(bookingEntity.getAddress()));
		}
		return booking;
	}
	
	private Address convert(AddressEntity addressEntity) {
		Address address = new Address();
		address.setId(addressEntity.getId());
		address.setCity(addressEntity.getCity());
		address.setCountry(addressEntity.getCountry());
		address.setLine1(addressEntity.getLine1());
		address.setLine2(addressEntity.getLine2());
		address.setState(addressEntity.getState());
		address.setZipCode(addressEntity.getZipCode());
		return address;
	}
	
	private BookingEntity convert(Booking booking) {
		BookingEntity bookingEntity = new BookingEntity();
		bookingEntity.setCheckin(booking.getCheckin());
		bookingEntity.setCheckout(booking.getCheckout());
		bookingEntity.setDateOfBirth(booking.getDateOfBirth());
		bookingEntity.setDeposit(booking.getDeposit());
		bookingEntity.setFirstName(booking.getFirstName());
		bookingEntity.setId(booking.getId());
		bookingEntity.setLastName(booking.getLastName());
		bookingEntity.setTotalprice(booking.getTotalprice());
		if(booking.getAddress()!=null) {
			bookingEntity.setAddress(convert(booking.getAddress()));
		}
		return bookingEntity;
	}
	
	private AddressEntity convert(Address address) {
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setId(address.getId());
		addressEntity.setCity(address.getCity());
		addressEntity.setCountry(address.getCountry());
		addressEntity.setLine1(address.getLine1());
		addressEntity.setLine2(address.getLine2());
		addressEntity.setState(address.getState());
		addressEntity.setZipCode(address.getZipCode());
		return addressEntity;
	}
	
}
