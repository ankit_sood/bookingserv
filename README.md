# bookingserv

## Application Overview
bookingserv is a spring boot rest application which would provide the CRUD operations for `Booking` resource.

There are three modules in this application
- bookingservApi - This module contains the interface.
    - `v1/schema/booking.json` defines the booking resource.
    - `jsonschema2pojo-maven-plugin` is being used to create `Booking POJO` from json file.
    - `BookingResource.java` is the interface for CRUD operations on `Booking` resource.
        - POST `/v1/bfs/booking` endpoint defined to create the resource.
- bookingservImplementation - This module contains the implementation for the rest endpoints.
    - `BookingResourceImpl.java` implements the `BookingResource` interface.
- bookingservFunctionalTests - This module would have the functional tests.

## How to run the application
- Please have Maven version `3.3.3` & Java 8 on your system.
- Use command `mvn clean install` to build the project.
- Use command `mvn spring-boot:run` from `bookingservImplementation` folder to run the project.
- Use postman or curl to access `http://localhost:8080/v1/bfs/booking` POST or GET endpoint.

## Sample Reqeust to create the booking
- HTTP Method: POST
- URL: http://localhost:8081/v1/bfs/booking
- Payload: {
    "first_name": "Ankit",
    "last_name": "Sood",
    "date_of_birth" : "1626599534000",
    "checkin": false,
    "checkout": false,
    "totalprice": 0.0,
    "deposit": 0.0,
    "address": {
        "line1": "SunCity Appartments",
        "city": "Bangalore",
        "state": "Karnataka",
        "country": "India",
        "zipCode": "560103"
    }
}

## Sample Reqeust to create the booking
- HTTP Method: GET
- URL: http://localhost:8081/v1/bfs/booking