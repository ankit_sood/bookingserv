package com.paypal.bfs.test.bookingserv.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "BOOKING")
public class BookingEntity implements Serializable {
	private static final long serialVersionUID = -7811314965329985541L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;
	
	@Column(name="CHECKIN")
	private Boolean checkin;
	
	@Column(name="CHECKOUT")
	private Boolean checkout;
	
	@Column(name="TOTAL_PRICE")
	private Double totalprice;
	
	@Column(name="DEPOSIT")
	private Double deposit;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ID")
	private AddressEntity address;
}
