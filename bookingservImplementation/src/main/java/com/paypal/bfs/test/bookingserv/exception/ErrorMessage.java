package com.paypal.bfs.test.bookingserv.exception;

import lombok.Data;

@Data
public class ErrorMessage {
	private String message;
	private boolean isSuccess;
	private String cause;
}
